package spaceships;

import java.util.ArrayList;


public class SpaceshipFleetManagerTest {

    CommandCenter commandCenter;
    ArrayList<Spaceship> testList = new ArrayList<>();

    public SpaceshipFleetManagerTest(CommandCenter commandCenter) {
        this.commandCenter = commandCenter;
    }

    public static void main(String[] args) {
        SpaceshipFleetManagerTest spaceshipFleetManagerTest = new SpaceshipFleetManagerTest(new CommandCenter());
        boolean[] tests = {
                spaceshipFleetManagerTest.getMostPowerfulShip_mostPowerfulShipExists_returnsIt(),
                spaceshipFleetManagerTest.getMostPowerfulShip_severalShips_returnsFirst(),
                spaceshipFleetManagerTest.getMostPowerfulShip_noAttackShips_returnsNull(),

                spaceshipFleetManagerTest.getShipByName_suchShipExists_returnsIt(),
                spaceshipFleetManagerTest.getShipByName_noSuchShip_returnsNull(),

                spaceshipFleetManagerTest.getAllCivilianShips_returnsAllCivilianShips(),
                spaceshipFleetManagerTest.getAllCivilianShips_noCivilians_returnsEmptyList(),

                spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_TwoRightShips_returnsThem(),
                spaceshipFleetManagerTest.getAllShipsWithEnoughCargoSpace_NoRightShips_returnsEmptyList()
        };
        double[] points = {0.25, 0.5, 0.25, 0.5, 0.5, 0.5, 0.5, 0.5, 0.5};
        double sum = 0.0;
        for (int i = 0; i < tests.length; i++) {
            System.out.print("test " + (i + 1));
            if (tests[i]) {
                System.out.println(" ok");
                sum += points[i];
            } else {
                System.out.println(" incorrect");
            }
        }
        System.out.println("your score - " + sum);
    }

    private boolean getMostPowerfulShip_mostPowerfulShipExists_returnsIt() {
        testList.clear();
        testList.add(new Spaceship("testship1", 100, 0, 0));
        testList.add(new Spaceship("testship2", 10, 0, 0));
        Spaceship testResult = commandCenter.getMostPowerfulShip(testList);
        return testResult.getFirePower() == 100 && testResult.getName().equals("testship1");
    }

    private boolean getMostPowerfulShip_severalShips_returnsFirst() {
        testList.clear();
        testList.add(new Spaceship("testship4", 100, 0, 0));
        testList.add(new Spaceship("testship1", 100, 0, 0));
        testList.add(new Spaceship("testship3", 100, 0, 0));
        testList.add(new Spaceship("testship2", 10, 0, 0));
        Spaceship testResult = commandCenter.getMostPowerfulShip(testList);
        return testResult.getName().equals("testship4");
    }

    private boolean getMostPowerfulShip_noAttackShips_returnsNull() {
        testList.clear();
        testList.add(new Spaceship("peaceful", 0, 1000, 0));
        testList.add(new Spaceship("peaceful", 0, 999, 0));
        testList.add(new Spaceship("peaceful", 0, 998, 0));
        Spaceship testResult = commandCenter.getMostPowerfulShip(testList);
        return testResult == null;
    }

    private boolean getAllCivilianShips_returnsAllCivilianShips() {
        testList.clear();
        testList.add(new Spaceship("c1", 0, 0, 0));
        testList.add(new Spaceship("c2", 0, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v2", 2, 0, 0));
        testList.add(new Spaceship("c3", 0, 0, 0));
        testList.add(new Spaceship("v4", 2, 0, 0));
        testList.add(new Spaceship("v5", 2, 0, 0));
        testList.add(new Spaceship("v6", 2, 0, 0));
        testList.add(new Spaceship("c4", 0, 0, 0));
        ArrayList<Spaceship> testResult = commandCenter.getAllCivilianShips(testList);
        ArrayList<Spaceship> intendedResult = new ArrayList<>();
        for (Spaceship spaceship : testList) {
            if (spaceship.getFirePower() == 0) intendedResult.add(spaceship);
        }
        return testResult.equals(intendedResult);
    }

    private boolean getAllCivilianShips_noCivilians_returnsEmptyList() {
        testList.clear();
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        testList.add(new Spaceship("v1", 2, 0, 0));
        ArrayList<Spaceship> testResult = commandCenter.getAllCivilianShips(testList);
        return testResult.equals(new ArrayList<>());
    }

    private boolean getAllShipsWithEnoughCargoSpace_TwoRightShips_returnsThem() {
        testList.clear();
        testList.add(new Spaceship("right", 0, 1000, 0));
        testList.add(new Spaceship("right", 0, 999, 0));
        testList.add(new Spaceship("wrong", 0, 998, 0));
        testList.add(new Spaceship("wrong", 0, 500, 0));
        testList.add(new Spaceship("wrong", 0, 500, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 999);
        ArrayList<Spaceship> intendedResult = new ArrayList<>();
        for (Spaceship spaceship : testList) {
            if (spaceship.getCargoSpace() >= 999) intendedResult.add(spaceship);
        }
        return result.equals(intendedResult);
    }

    private boolean getAllShipsWithEnoughCargoSpace_NoRightShips_returnsEmptyList() {
        testList.clear();
        testList.add(new Spaceship("wrong", 0, 500, 0));
        testList.add(new Spaceship("wrong", 0, 500, 0));
        testList.add(new Spaceship("wrong", 0, 500, 0));
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList, 999);
        return result.equals(new ArrayList<>());
    }

    private boolean getShipByName_suchShipExists_returnsIt() {
        testList.clear();
        testList.add(new Spaceship("buba", 0, 0, 0));
        testList.add(new Spaceship("biba", 0, 0, 0));
        testList.add(new Spaceship("boba", 0, 0, 0));
        testList.add(new Spaceship("dima", 0, 0, 0));
        return commandCenter.getShipByName(testList, "boba").equals(testList.get(2));
    }

    private boolean getShipByName_noSuchShip_returnsNull() {
        testList.clear();
        testList.add(new Spaceship("buba", 0, 0, 0));
        testList.add(new Spaceship("biba", 0, 0, 0));
        testList.add(new Spaceship("boba", 0, 0, 0));
        testList.add(new Spaceship("dima", 0, 0, 0));
        return commandCenter.getShipByName(testList, "") == null;
    }

}
