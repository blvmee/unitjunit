package spaceships;

import org.junit.jupiter.api.*;
import java.util.ArrayList;

public class SpaceshipFleetManagerTestButWithExternalLibrary {

    CommandCenter commandCenter = new CommandCenter();
    static ArrayList<Spaceship> testList = new ArrayList<>();

    @BeforeEach
    void fill() {
        testList.add(new Spaceship("тотя",20,35,1));
        testList.add(new Spaceship("лоля",0,30,4));
        testList.add(new Spaceship("лотя",20,0,8));
        testList.add(new Spaceship("толя",0,0,8));
    }

    @AfterEach
    void wipe() {
        testList.clear();
    }

    @DisplayName("getMostPowerfulShip_mostPowerfulShipExists_returnsIt")
    @Test
    void test1() {
        Assertions.assertEquals(20, commandCenter.getMostPowerfulShip(testList).getFirePower());
    }

    @DisplayName("getMostPowerfulShip_severalShips_returnsFirst")
    @Test
    void test2() {
        Assertions.assertEquals("тотя", commandCenter.getMostPowerfulShip(testList).getName());
    }

    @DisplayName("getMostPowerfulShip_noAttackShips_returnsNull")
    @Test
    void test3() {
        testList.removeIf(spaceship -> spaceship.getFirePower() > 0);
        Assertions.assertNull(commandCenter.getMostPowerfulShip(testList));
    }

    @DisplayName("getAllCivilianShips_returnsAllCivilianShips")
    @Test
    void test4() {
        ArrayList<Spaceship> result = commandCenter.getAllCivilianShips(testList);
        testList.removeIf(spaceship -> spaceship.getFirePower() > 0);
        Assertions.assertEquals(result, testList);
    }

    @DisplayName("getAllCivilianShips_noCivilians_returnsEmptyList")
    @Test
    void test5() {
        testList.removeIf(spaceship -> spaceship.getFirePower() == 0);
        Assertions.assertEquals(new ArrayList<>(), commandCenter.getAllCivilianShips(testList));
    }

    @DisplayName("getAllShipsWithEnoughCargoSpace_TwoRightShips_returnsThem")
    @Test
    void test6() {
        ArrayList<Spaceship> result = commandCenter.getAllShipsWithEnoughCargoSpace(testList,5);
        testList.removeIf(spaceship -> spaceship.getCargoSpace() < 5);
        Assertions.assertEquals(result, testList);
    }

    @DisplayName("getAllShipsWithEnoughCargoSpace_NoRightShips_returnsEmptyList")
    @Test
    void test7() {
        testList.removeIf(spaceship -> spaceship.getCargoSpace() < 400000);
        Assertions.assertEquals(new ArrayList<>(), testList);
    }

    @DisplayName("getShipByName_suchShipExists_returnsIt")
    @Test
    void test8() {
        Assertions.assertEquals(testList.get(3), commandCenter.getShipByName(testList, "толя"));
    }

    @DisplayName("getShipByName_noSuchShip_returnsNull")
    @Test
    void test9() {
        Assertions.assertNull(commandCenter.getShipByName(testList, "Максим"));
    }

}