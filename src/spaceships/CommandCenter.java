package spaceships;


import java.util.ArrayList;

// Вы проектируете интеллектуальную систему управления ангаром командного центра.
// Реализуйте интерфейс SpaceshipFleetManager для управления флотом кораблей.
// Используйте СУЩЕСТВУЮЩИЙ интерфейс и класс космического корабля (SpaceshipFleetManager и Spaceship).
public class CommandCenter implements SpaceshipFleetManager {

    @Override
    public Spaceship getMostPowerfulShip(ArrayList<Spaceship> ships) {
        int index = 0;
        int maxPower = ships.get(0).getFirePower();
        for (int i = 0; i < ships.size(); i++) {
            Spaceship spaceship = ships.get(i);
            if (spaceship.getFirePower() > maxPower) {
                maxPower = spaceship.getFirePower();
                index = i;
            }
        }
        if (maxPower == 0) return null;
        return ships.get(index);
    }

    @Override
    public Spaceship getShipByName(ArrayList<Spaceship> ships, String name) {
        Spaceship shipToFind = null;
        for (Spaceship spaceship : ships) {
            if (spaceship.getName().equals(name)) {
                shipToFind = spaceship;
                break;
            }
        }
        return shipToFind;
    }

    @Override
    public ArrayList<Spaceship> getAllShipsWithEnoughCargoSpace(ArrayList<Spaceship> ships, Integer cargoSize) {
        ships.removeIf(spaceship -> spaceship.getCargoSpace() < cargoSize);
        return ships;
    }

    @Override
    public ArrayList<Spaceship> getAllCivilianShips(ArrayList<Spaceship> ships) {
        ships.removeIf(spaceship -> spaceship.getFirePower() != 0);
        return ships;
    }
}
